package eu.telecomnancy.ui;

import eu.telecomnancy.CommandeGetValue;
import eu.telecomnancy.CommandeOff;
import eu.telecomnancy.CommandeOn;
import eu.telecomnancy.CommandeUpdate;
import eu.telecomnancy.sensor.*;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

public class SensorView extends JPanel implements Observer{
    private ISensor sensor;

    private JLabel value = new JLabel("N/A °C");
    private JButton on = new JButton("On");
    private JButton off = new JButton("Off");
    private JButton update = new JButton("Acquire");
    
    public SensorView(ISensor c) {
    	((TemperatureSensor)c).addObserver(this);
        this.sensor = c;
        this.setLayout(new BorderLayout());

        value.setHorizontalAlignment(SwingConstants.CENTER);
        Font sensorValueFont = new Font("Sans Serif", Font.BOLD, 18);
        value.setFont(sensorValueFont);

        this.add(value, BorderLayout.CENTER);

        CommandeGetValue getval=new CommandeGetValue(sensor);

        final CommandeOn ComOn=new CommandeOn(sensor);

        final CommandeOff ComOff=new CommandeOff(sensor);

        final CommandeUpdate ComUpdate=new CommandeUpdate(sensor);
        on.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ComOn.execute();
            }
        });

        off.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ComOff.execute();
            }
        });

        update.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ComUpdate.execute();
            }
        });

        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new GridLayout(1, 3));
        buttonsPanel.add(update);
        buttonsPanel.add(on);
        buttonsPanel.add(off);

        this.add(buttonsPanel, BorderLayout.SOUTH);
    }

	@Override
	public void update(Observable arg0, Object arg1) {
		this.sensor=((ISensor) arg1);
		
	}
}
