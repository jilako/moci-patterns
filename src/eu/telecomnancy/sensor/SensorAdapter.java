package eu.telecomnancy.sensor;

import java.util.Observable;
import java.util.Random;

public class SensorAdapter extends Observable implements ISensor{

	LegacyTemperatureSensor legacy=new LegacyTemperatureSensor();
	public SensorAdapter(LegacyTemperatureSensor legacy){
		this.legacy=legacy;
	}
	@Override
	public void on() {
		boolean status = this.legacy.getStatus();
		if(status==true){
			this.legacy.onOff();
			this.setChanged();
			this.notifyObservers();
		}
	}

	@Override
	public void off() {
		boolean status = this.legacy.getStatus();
		if(status!=true){
			this.legacy.onOff();
			this.setChanged();
			this.notifyObservers();
		}

	}

	@Override
	public boolean getStatus() {
		return this.legacy.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException {
		if (this.legacy.getStatus()){
			this.legacy.onOff();
			this.legacy.onOff();
		
	}
		else this.legacy.onOff();
		this.setChanged();
		this.notifyObservers();
	}

	@Override
	public double getValue() throws SensorNotActivatedException {

		return   this.legacy.getTemperature();
	}



}
