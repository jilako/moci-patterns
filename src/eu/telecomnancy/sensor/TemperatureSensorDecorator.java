package eu.telecomnancy.sensor;

import java.util.Random;

public abstract class TemperatureSensorDecorator implements ISensor {
	boolean state;
	protected ISensor temperatureDecorator;
	double value = 0;
	public TemperatureSensorDecorator(ISensor temp) {
		this.temperatureDecorator=temp;
	}
	 	@Override
	public void on() {
		temperatureDecorator.on();
	}
	@Override
	public void off() {
		temperatureDecorator.off();
	}
	@Override
	public boolean getStatus() {
		return temperatureDecorator.getStatus();
	}
	@Override
	public void update() throws SensorNotActivatedException {
		temperatureDecorator.update();
		}
	@Override
	public double getValue() throws SensorNotActivatedException {
		return temperatureDecorator.getValue();
	}
}
class TemperatureSensorArrondi extends TemperatureSensorDecorator{

	public TemperatureSensorArrondi(ISensor temp) {
		super(temp);
		// TODO Auto-generated constructor stub
	}
	public double getValue() throws SensorNotActivatedException{
		int x =(int)Math.floor( super.getValue());
		return x;
	}
	
}
class TemperatureSensorFahrenheit extends TemperatureSensorDecorator{

	public TemperatureSensorFahrenheit(ISensor temp) {
		super(temp);
		// TODO Auto-generated constructor stub
	}
	public double getValue() throws SensorNotActivatedException{
		double x =super.getValue();
		return x*1.8+32;
	}
}
