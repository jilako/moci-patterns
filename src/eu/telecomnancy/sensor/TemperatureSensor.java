package eu.telecomnancy.sensor;

import java.util.Observable;
import java.util.Random;

public class TemperatureSensor extends Observable implements ISensor {
    StateSensor state=null;
    double value = 0;

    @Override
    public void on() {
        state = new TemperatureSensorOn();
        this.setChanged();
        this.notifyObservers();
    }

    @Override
    public void off() {
        state = new TemperatureSensorOff();
        this.setChanged();
        this.notifyObservers();
    }

    @Override
    public boolean getStatus() {
        return state.getStatus();
    }

    @Override
    public void update() throws SensorNotActivatedException {
       /* if (state){
        	value = (new Random()).nextDouble() * 100;
            this.setChanged();
            this.notifyObservers();
        }
            
        else throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
   */
    state.update();	
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
      return state.getValue();
       }

    
}
