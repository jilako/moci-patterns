package eu.telecomnancy.sensor;

import java.util.Observable;
import java.util.Random;

public class TemperatureSensorOff extends Observable implements StateSensor {
    boolean state=false;
    double value = 0;



    @Override
    public boolean getStatus() {
        return state;
    }

    @Override
    public void update() throws SensorNotActivatedException {
    	throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
    	throw new SensorNotActivatedException("Sensor must be activated to get its value.");
    }

}
