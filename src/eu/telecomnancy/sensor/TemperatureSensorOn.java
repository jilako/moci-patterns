package eu.telecomnancy.sensor;

import java.util.Observable;
import java.util.Random;

public class TemperatureSensorOn extends Observable implements StateSensor {
    boolean state=true;
    double value = 0;

    @Override
    public boolean getStatus() {
        return state;
    }

    @Override
    public void update(){
        	value = (new Random()).nextDouble() * 100;
            this.setChanged();
            this.notifyObservers();
      }

    @Override
    public double getValue() throws SensorNotActivatedException {

            return value;
     }

}
