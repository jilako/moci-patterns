package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class CommandeGetValue {
	public CommandeGetValue(ISensor sensor) {
		super();
		this.sensor = sensor;
	}
	ISensor sensor;
	public double execute() {
		try {
			return sensor.getValue();
		} catch (SensorNotActivatedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return -500;
	}

}
