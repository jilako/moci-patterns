package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;

public class CommandeOn  implements Command{
	public CommandeOn(ISensor sensor) {
		super();
		this.sensor = sensor;
	}
	ISensor sensor;
	@Override
	public void execute() {
		sensor.on();
	}

}
