package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class CommandeUpdate  implements Command{
	ISensor sensor;
	public CommandeUpdate(ISensor sensor) {
		super();
		this.sensor = sensor;
	}
	@Override
	public void execute() {
		try {
			sensor.update();
		} catch (SensorNotActivatedException e) {
			
		}
	}

}
