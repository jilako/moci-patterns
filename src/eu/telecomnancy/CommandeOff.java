package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;

public class CommandeOff implements Command {
	ISensor sensor;
	public CommandeOff(ISensor sensor) {
		super();
		this.sensor = sensor;
	}
	@Override
	public void execute() {
		sensor.off();
	}

}
