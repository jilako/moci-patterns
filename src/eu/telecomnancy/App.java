package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.LegacyTemperatureSensor;
import eu.telecomnancy.sensor.SensorAdapter;
import eu.telecomnancy.sensor.TemperatureSensor;
import eu.telecomnancy.ui.ConsoleUI;

public class App {

    public static void main(String[] args) {
        //ISensor sensor = new TemperatureSensor();
        LegacyTemperatureSensor legacy=new LegacyTemperatureSensor();
        ISensor capteur= new SensorAdapter(legacy);
        //new ConsoleUI(sensor);
        new ConsoleUI(capteur);
    }

}